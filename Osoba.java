/*
 * Klasa mająca na celu wskazanie jak działa obiektowość. Plik ten można podłączyć do dowolnego pgoramu w języku Java - 
 * wystarczy umieścić go w zródłach i/lub podłączyć do projektu. Każdy plik klasy MUSI posiadać przynajmniej jedną publiczną klasę (inne 
 * możliwe modyfikatory poznane zostaną pózniej). 
 */

/*
 * NALEŻY DODAĆ!:
 * - adres zamieszkania (ulica, numer domu, mieszkania, kod pocztowy oraz miejscowosc); 
 * najlepiej możliwość podawania każdej informacji oddzielnie (set/get) jak i wszystkich łącznie w jednej funkcji (jako ciąg znakowy
 * bądż jako wartości wymieniane po przecinku, w odpowiedniej kolejności)
 * - numer telefonu (nalezy sprawdzać, czy użytkownik podał odpowiednią liczbe znaków (minimum to 9 dla polskich numerów); metoda powinna
 * domyślnie dodawać +48 dla poslkich numerów; ponadto numer powinien być zapisywany z odpowiednim rozstrzeleniem wartości (poprzez
 * myślniki lub spacje) 
 * - dodac typ wyliczeniowy dla uperwanień:
 * a) Administrator
 * b) Pracownik
 * c) Magazynier
 * d) Brak
 */

public class Osoba {
		//poniżej jest przykład konstruktora klasy; konstruktory to metody (funkcje), jednak nie posiadają one standardowego
		//zwrócenia wartości (takiego jak void, int czy float). Konstruktory pozwalają na ustawienia początkowe niektórych
	    //własności nowego obiektu tworzonego na podstawie klasy (powinny w zasadzie ustawiać wszystkie własności). 
		//Jeżeli sami nie zdefiniujemy konstrutora, zostanie on stworzony niejawnie (bez naszej wiedzy i "zgody"). Taki domyślny konstruktor
		//(tak się go nazywa) będzie zerował wszystkie zmienne (lub ustawiał je na wartość null). Poniższy konstrutor robi doładnie 
		//to co robiłby konstruktor domyślny - tutaj jednak "sami" o tym decydujemy
		public Osoba() {
			imie=nazwisko=pesel=null;
			uprawnienia=0;
		} 
		
		public boolean setImie(String i) {
			if (i.length()<2) return false;
			if (i.charAt(0) < 'A' || i.charAt(0) > 'Z') return false;
			imie = i;
			return true;
		}
		
		public String getImie() {
			return imie;
		}
		
		public boolean setNazwisko(String i) {
			if (i.length()<2) return false;
			if (i.charAt(0) < 'A' || i.charAt(0) > 'Z') return false;
			nazwisko = i;
			return true;
		}
		
		public String getNazwisko() {
			return nazwisko;
		}
		
		public boolean setPESEL(String p) {
			if (p.length() != 11) return false;
		    if (Integer.valueOf(String.valueOf(String.valueOf(p.charAt(4)) + p.charAt(5))) > 31) return false; 
		    if (String.valueOf(String.valueOf(p.charAt(0)) + p.charAt(1)) == "88") return false; 
		    if (String.valueOf(String.valueOf(p.charAt(2)) + p.charAt(2)) == "88") return false; 
		    if (String.valueOf(String.valueOf(p.charAt(3)) + p.charAt(4)) == "88") return false; 
		  //1×a + 3×b + 7×c + 9×d + 1×e + 3×f + 7×g + 9×h + 1×i + 3×j + 1×k
		    int c = (Integer.valueOf(p.charAt(0)) + 3*Integer.valueOf(p.charAt(1))+ 7*Integer.valueOf(p.charAt(2)) +
		    		9*Integer.valueOf(p.charAt(3)) + Integer.valueOf(p.charAt(4)) + 3*Integer.valueOf(p.charAt(5)) +
		    		7*Integer.valueOf(p.charAt(6))+9*Integer.valueOf(p.charAt(7))+Integer.valueOf(p.charAt(8))+
		    		3*Integer.valueOf(p.charAt(9))+Integer.valueOf(p.charAt(10))) % 10;
		    if (c != 0) return false;
		   pesel=p;		    		
		    //pesel = ss;
		    return true;
		}
		
		
		
		public String getPESEL() {
			return pesel;
		}
		
		public boolean setUprawnienia(int u) {
			if (u<0 || u>2) return false;
			uprawnienia=u;
			return true;
		}
		
		public int getUprawnienia() {
			return uprawnienia;
		}
		
		/*
		 * Modyfikator private powoduje, że pola klasy są widoczne tylko w niej samej. Nikt, kto zadeklaruje zmienną 
		 * stworzoną z klasy nie będzie mógł bezpośrednio zapisać czegoś do takiej zmiennej ani też z niej nic odczytać
		 * Jedynie metody (funkcje) tej klasy będą mogły teog dokonywać. Stąd utworzone metody setImie/getImie, które, będąc
		 * publicznymi zmiennymi, pozwalają na zapis do zmiennej imie oraz odczyt z niej. Nazwy stworzone są wedle konwencji
		 * programistycznej, jednak mogą brzmieć dowolnie. Ważne jest jedynie, że tylko przez nie można modyfikować/czytać
		 * wartość zmiennej imie. 
		 * Podobne zestawy funkcji trzeba tworzyć dla każdej zmiennej z osobna (o ile zachodzi konieczność modyfikacji lub odczytu
		 * takich zmiennych). Należy bowiem pamiętac, że niektóre zmienne mogą być wręcz zbędnie wystawiane do odczytu (mają działać jedynie
		 * wewnętrz danej klasy) lub być odczytywane/zapisywane wraz z innymi danymi. Dodatkowo przy zapisie/odczycie mogą
		 * musieć spełniać dodatkowe warunki (tak jak jest w przypadku zapisu imienia).
		 */
		private String imie,nazwisko;
		private String pesel;
		private int uprawnienia;
	
	}